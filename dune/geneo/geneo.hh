#ifndef DUNE_GENEO_HH
#define DUNE_GENEO_HH


#include <dune/geneo/two_level_schwarz.hh>

#include <dune/geneo/subdomainprojectedcoarsespace.hh>

#include <dune/geneo/partitionofunity.hh>
#include <dune/geneo/localoperator_ovlp_region.hh>

#include <dune/geneo/geneobasis.hh>
#include <dune/geneo/liptonbabuskabasis.hh>
#include <dune/geneo/partitionofunitybasis.hh>
#include <dune/geneo/zembasis.hh>


#endif // DUNE_GENEO_HH
