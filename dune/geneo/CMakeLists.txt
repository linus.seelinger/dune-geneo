#install headers
install(FILES
  arpackpp_fork.hh
  coarsespace.hh
  geneo.hh
  multicommdatahandle.hh
  partitionofunity.hh
  subdomainbasis.hh
  zembasis.hh
  geneobasis.hh
  localoperator_ovlp_region.hh
  partitionofunitybasis.hh
  partitionofunityspace.hh
  two_level_schwarz.hh

  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/geneo)
