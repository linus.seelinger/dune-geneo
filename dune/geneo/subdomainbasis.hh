
#ifndef DUNE_GENEO_SUBDOMAINBASIS_HH
#define DUNE_GENEO_SUBDOMAINBASIS_HH


template <class X>
class SubdomainBasis {

public:

  std::vector<std::shared_ptr<X> > local_basis; // Local coarse space basis

  /*  virtual std::shared_ptr<COARSE_V> restrict_defect (const X& d) const = 0;

  virtual std::shared_ptr<X> prolongate_defect (const COARSE_V& v0) const = 0;

  virtual std::shared_ptr<Dune::UMFPack<COARSE_M>> get_coarse_solver () = 0;

  virtual int basis_size() = 0;*/

};




#endif //DUNE_GENEO_SUBDOMAINBASIS_HH
