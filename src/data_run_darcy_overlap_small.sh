#!/bin/sh


global_settings="
-solver geneo
-coarse_exclusive_solver false

-coarseSpaceActive false
-eigenvectors 0
-eigenvectors_compute 30
-threshold_eigenvectors false
-EVboundariesFromProblem true
-AddPartUnityToEigenvectors false

-OldMatrixSetup false
-widlund_part_unity false

-ConditionEstimate true
-criterion defect
-OrthogonalizeCG false


-Grid.cells 200
-Grid.overlap 1
-Grid.partition_x 4
-Grid.partition_y 4
-Grid.partition_z 1

-Problem1.perm1 1.0
-Problem1.perm2 1e3
-Problem1.layers 25

-Problem4.perm1 1.0
-Problem4.perm2 1.0
-Problem4.layers 25
-Problem4.theta1 0.0
-Problem4.theta2 45
-Problem4.eps1 1e-3
-Problem4.eps2 1e-3
"

# Write first CSV row
echo "Layers;Iterations;Condition;" > results.csv;


#if false; then


echo "one-level;" >> results.csv;

for overlap in 1 2 4 8
do

  # User info output

  echo "running ${overlap}";


  # Run program

  mpirun -np 16 ./dune-geneo $global_settings -Grid.overlap ${overlap} -coarseSpaceActive false -eigenvectors 0 > out;


  # Extract data from output, write to CSV

  echo -n "${overlap}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done


#fi

echo "geneo threshold;" >> results.csv;

for overlap in 1 2 4 8
do

  # User info output

  echo "running ${overlap}";


  # Run program

  mpirun -np 16 ./dune-geneo $global_settings -Grid.overlap ${overlap} -coarseSpaceActive true -eigenvectors 10 -threshold_eigenvectors true > out;


  # Extract data from output, write to CSV

  echo -n "${overlap}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done



#fi

for eigenvectors in 1 2 4 8 16 #2 3 # 4 5 6 7 8 9 10
do

echo "geneo eigenvectors: ${eigenvectors};" >> results.csv;

for overlap in 1 2 4 8
do

  # User info output

  echo "running ${overlap}";


  # Run program

  mpirun -np 16 ./dune-geneo $global_settings -Grid.overlap ${overlap} -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "${overlap}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

