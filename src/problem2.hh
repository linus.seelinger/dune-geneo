/** Parameter class for the stationary convection-diffusion equation of the following form:
 *
 * \f{align*}{
 *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \ \
 *                                              u &=& g \mbox{ on } \partial\Omega_D (Dirichlet)\ \
 *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N (Flux)\ \
 *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O (Outflow)
 * \f}
 * Note:
 *  - This formulation is valid for velocity fields which are non-divergence free.
 *  - Outflow boundary conditions should only be set on the outflow boundary
 *
 * The template parameters are:
 *  - GV a model of a GridView
 *  - RF numeric type to represent results
 */

#include <math.h>       /* sin */

template<typename GV, typename RF>
class GenericEllipticProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {

    typename Traits::DomainType xglobal = e.geometry().global(x);
    int num1 = floor(8*xglobal[0]);
    int num2 = floor(8*xglobal[1]);
    RF coeff = 0.0;
    if ( (num1 % 2 == 0) && (num2 % 2 == 0) )
      coeff = 1e5*(num2+1.0);
    else
      coeff = 1.0;

    RF duct1_b = 0.9*xglobal[0];
    RF duct1_t = duct1_b + 0.1;
    if ( (xglobal[1]>duct1_b) && (xglobal[1]<duct1_t))
      coeff = coeff + 9.5E5;

    RF duct2_b = -1.0*xglobal[0] + 0.5;
    RF duct2_t = duct2_b + 0.1;
    if ( (xglobal[1]>duct2_b) && (xglobal[1]<duct2_t))
      coeff = coeff + 9.5E5;

    RF duct3_b = 4.0*xglobal[0] - 2.0 ;
    RF duct3_t = duct3_b + 0.1;
    if ( (xglobal[1]>duct3_b) && (xglobal[1]<duct3_t))
      coeff = coeff + 9.5E5;

    std::ofstream myfile;
    myfile.open("conductivity.txt", std::ios_base::app);
    myfile << std::left << std::setw(12) << xglobal[0] << std::left << std::setw(12) << xglobal[1] << std::left << std::setw(1)  << coeff << "\n";
    myfile.close();

    RF eps = 1.0;
    RF th = 0;
    typename Traits::PermTensorType I;
    th=th*M_PI/180.0;
    I[0][0]=coeff*(pow(cos(th),2.0) + pow(sin(th),2.0)*eps);
    I[0][1]=coeff*sin(2.0*th)*(eps-1.0)/2.0;
    I[1][0]=coeff*sin(2.0*th)*(eps-1.0)/2.0;
    I[1][1]=coeff*(pow(sin(th),2.0) + pow(cos(th),2.0)*eps);
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! boundary condition type function
  /* return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet for Dirichlet boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann for flux boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow for outflow boundary conditions
   */
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(x);
    if(xglobal[0]<1E-6 || xglobal[0]>1.0-1E-6)
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
    else
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    if (xglobal[1] > 1.0-1E-6)
      return 1.0;
    else
      return 0.0;
  }

  //! flux boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
};
