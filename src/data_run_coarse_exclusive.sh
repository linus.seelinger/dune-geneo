#!/bin/sh


# Write first CSV row
echo "EVs;Error;" > results.csv;



for EVs in 1 2 3 4 5 6 7 8 9 10
do

  # User info output

  echo "running ${EVs} EVs";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarse_exclusive_solver true -coarseSpaceActive true -eigenvectors ${EVs} -threshold_eigenvectors false -ZEM_from_arpack false > out;


  # Extract data from output, write to CSV

  echo -n "${EVs}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Error norm: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done


