#!/bin/sh


# Write first CSV row
echo "Angle;Anisotropy;Iterations;Condition;" > results.csv;


#if false; then
#fi
if false; then

echo "AMG;" >> results.csv;


for angle2 in 0 1 3 5 10 15 45 70 90
do
for aniso2_i in -1 -3 -5 #4 6 9
do

  # User info output

  echo "running 1e${angle2}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver amg -coarseSpaceActive false -eigenvectors 0 -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.aniso2 "1e${aniso2_i}" -Problem4.angle2 "${angle2}" > out;


  # Extract data from output, write to CSV

  echo -n "${angle2}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n "1e${aniso2_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done
done



echo "one-level;" >> results.csv;

for angle2 in 0 1 3 5 10 15 45 70 90
do
for aniso2_i in -1 -3 -5 #4 6 9
do

  # User info output

  echo "running 1e${angle2}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive false -eigenvectors 0 -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.aniso2 "1e${aniso2_i}" -Problem4.angle2 "${angle2}" > out;


  # Extract data from output, write to CSV

  echo -n "${angle2}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n "1e${aniso2_i}" >> results.csv;
  echo -n ";" >> results.csv;


  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done
done



echo "ZEM;" >> results.csv;

for angle2 in 0 1 3 5 10 15 45 70 90
do
for aniso2_i in -1 -3 -5 #4 6 9
do


  # User info output

  echo "running 1e${angle2}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors 2 -threshold_eigenvectors false -ZEM_from_arpack true -Problem4.aniso2 "1e${aniso2_i}" -Problem4.angle2 "${angle2}" > out;


  # Extract data from output, write to CSV

  echo -n "${angle2}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n "1e${aniso2_i}" >> results.csv;
  echo -n ";" >> results.csv;


  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done
done


echo "geneo threshold;" >> results.csv;

for angle2 in 0 1 3 5 10 15 45 70 90
do
for aniso2_i in -1 -3 -5 #4 6 9
do


  # User info output

  echo "running 1e${angle2}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors 5 -threshold_eigenvectors true -ZEM_from_arpack false -Problem4.aniso2 "1e${aniso2_i}" -Problem4.angle2 "${angle2}" > out;


  # Extract data from output, write to CSV

  echo -n "${angle2}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n "1e${aniso2_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done
done


echo "part unity coarse space;" >> results.csv;

for angle2 in 0 1 3 5 10 15 45 70 90
do
for aniso2_i in -1 -3 -5 #4 6 9
do


  # User info output

  echo "running 1e${angle2}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors 0 -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.aniso2 "1e${aniso2_i}" -Problem4.angle2 "${angle2}" > out;


  # Extract data from output, write to CSV

  echo -n "${angle2}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n "1e${aniso2_i}" >> results.csv;
  echo -n ";" >> results.csv;


  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done
done


fi


for eigenvectors in 10
do

echo "geneo eigenvectors: ${eigenvectors};" >> results.csv;

for angle2 in 0 1 3 5 10 15 45 70 90
do
for aniso2_i in -1 -3 -5 #4 6 9
do

  # User info output

  echo "running 1e${angle2}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.aniso2 "1e${aniso2_i}" -Problem4.angle2 "${angle2}" > out;


  # Extract data from output, write to CSV

  echo -n "${angle2}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n "1e${aniso2_i}" >> results.csv;
  echo -n ";" >> results.csv;


  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done
done

done



