#!/bin/sh


global_settings="
-solver geneo
-coarse_exclusive_solver false

-coarseSpaceActive false
-eigenvectors 0
-eigenvectors_compute 30
-threshold_eigenvectors false
-EVboundariesFromProblem true
-AddPartUnityToEigenvectors false

-OldMatrixSetup true
-widlund_part_unity false

-ConditionEstimate true
-criterion defect
-OrthogonalizeCG false


-Grid.cells 10
-Grid.overlap 1
-Grid.partition_x 1
-Grid.partition_y 1
-Grid.partition_z 1


-Problem1_3D.perm1 1.0
-Problem1_3D.perm2 1e3
-Problem1_3D.layers 4

"

# Need to set dim = 3 in dune-geneo.cc and use problem1_3d.hh !!!


# Write first CSV row
echo "Subdomains;Iterations;Condition;" > results.csv;


#if false; then


echo "one-level;" >> results.csv;

for subdomains in 2 4 8 16
do

  # User info output

  echo "running ${subdomains}";


  # Run program

  mpirun -np ${subdomains} ./dune-geneo $global_settings -Grid.partition_z ${subdomains} -coarseSpaceActive false -eigenvectors 0 > out;

  # Extract data from output, write to CSV

  echo -n "${subdomains}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

echo "AMG;" >> results.csv;

for subdomains in 2 4 8 16
do

  # User info output

  echo "running ${subdomains}";


  # Run program

  mpirun -np ${subdomains} ./dune-geneo $global_settings -Grid.partition_z ${subdomains} -solver amg -coarseSpaceActive false -eigenvectors 0 > out;


  # Extract data from output, write to CSV

  echo -n "${subdomains}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done


#fi


echo "geneo threshold;" >> results.csv;

for subdomains in 2 4 8 16
do

  # User info output

  echo "running ${subdomains}";


  # Run program

  mpirun -np ${subdomains} ./dune-geneo $global_settings -Grid.partition_z ${subdomains} -coarseSpaceActive true -eigenvectors 20 -threshold_eigenvectors true > out;


  # Extract data from output, write to CSV

  echo -n "${subdomains}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done



#fi

for eigenvectors in 1 2 3 4 5 6 7 10
do

echo "geneo eigenvectors: ${eigenvectors};" >> results.csv;

for subdomains in 2 4 8 16
do

  # User info output

  echo "running ${subdomains}";


  # Run program

  mpirun -np ${subdomains} ./dune-geneo $global_settings -Grid.partition_z ${subdomains} -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "${subdomains}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

