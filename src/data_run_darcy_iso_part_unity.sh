#!/bin/sh


global_settings="
-solver geneo
-coarse_exclusive_solver false

-coarseSpaceActive true
-eigenvectors 4
-eigenvectors_compute 15
-threshold_eigenvectors false
-EVboundariesFromProblem true
-AddPartUnityToEigenvectors false

-OldMatrixSetup true
-widlund_part_unity false

-ConditionEstimate true
-criterion defect
-OrthogonalizeCG false


-Grid.cells 125
-Grid.overlap 1
-Grid.partition_x 5
-Grid.partition_y 5
-Grid.partition_z 1


-Problem1.perm1 1.0
-Problem1.perm2 1e3
-Problem1.layers 25

-Problem2.contrast 1e3
"

# Write first CSV row
echo "Widlund;Iterations;Condition;" > results.csv;


#if false; then

echo "Threshold EV;" >> results.csv;

for overlap in 1 2 3 4 5
do

echo "overlap: ${overlap};" >> results.csv;

for widlund_part_unity in true false
do

  # User info output

  echo "running ${widlund_part_unity}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -widlund_part_unity $widlund_part_unity -Grid.overlap ${overlap} -eigenvectors 10 -threshold_eigenvectors true > out;


  # Extract data from output, write to CSV

  echo -n "${widlund_part_unity}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

#fi



echo "4 EV;" >> results.csv;

for overlap in 1 2 3 4 5
do

echo "overlap: ${overlap};" >> results.csv;

for widlund_part_unity in true false
do

  # User info output

  echo "running ${widlund_part_unity}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -widlund_part_unity $widlund_part_unity -Grid.overlap ${overlap} -eigenvectors 4 -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "${widlund_part_unity}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

