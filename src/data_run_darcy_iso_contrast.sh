#!/bin/sh


global_settings="
-solver geneo
-coarse_exclusive_solver false

-coarseSpaceActive false
-eigenvectors 0
-eigenvectors_compute 30
-threshold_eigenvectors false
-EVboundariesFromProblem true
-AddPartUnityToEigenvectors false

-OldMatrixSetup true
-widlund_part_unity false

-ConditionEstimate true
-criterion defect
-OrthogonalizeCG false


-Grid.cells 125
-Grid.overlap 1
-Grid.partition_x 5
-Grid.partition_y 5
-Grid.partition_z 1


-Problem1.perm1 1.0
-Problem1.perm2 1e3
-Problem1.layers 25

-Problem2.contrast 1e3
"

# Write first CSV row
echo "Layers;Iterations;Condition;" > results.csv;


#if false; then


echo "one-level;" >> results.csv;

for contrast in 0 1 2 3 4 5
do

  # User info output

  echo "running 1e${contrast}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Problem1.perm2 1e${contrast} -Problem2.contrast 1e${contrast} -coarseSpaceActive false -eigenvectors 0 > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done


echo "geneo threshold;" >> results.csv;

for contrast in 0 1 2 3 4 5
do

  # User info output

  echo "running 1e${contrast}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Problem1.perm2 1e${contrast} -Problem2.contrast 1e${contrast} -coarseSpaceActive true -eigenvectors 10 -threshold_eigenvectors true > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done



#fi

for eigenvectors in 1 2 3 4 5 6 7
do

echo "geneo eigenvectors: ${eigenvectors};" >> results.csv;

for contrast in 0 1 2 3 4 5
do

  # User info output

  echo "running 1e${contrast}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Problem1.perm2 1e${contrast} -Problem2.contrast 1e${contrast} -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

