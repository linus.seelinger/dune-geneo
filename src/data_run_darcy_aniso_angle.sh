#!/bin/sh


global_settings="
-solver geneo
-coarse_exclusive_solver false

-coarseSpaceActive false
-eigenvectors 0
-eigenvectors_compute 40
-threshold_eigenvectors false
-EVboundariesFromProblem true
-AddPartUnityToEigenvectors false

-OldMatrixSetup false
-widlund_part_unity false

-ConditionEstimate true
-criterion defect
-OrthogonalizeCG false


-Grid.cells 125
-Grid.overlap 2
-Grid.partition_x 5
-Grid.partition_y 5
-Grid.partition_z 1


-Problem4.perm1 1.0
-Problem4.perm2 1.0
-Problem4.layers 25
-Problem4.theta1 0.0
-Problem4.theta2 0.0
-Problem4.eps1 1e-2
-Problem4.eps2 1e-2
"

# Write first CSV row
echo "Layers;Iterations;Condition;" > results.csv;


#if false; then


echo "one-level;" >> results.csv;

for theta in 0 1 3 5 10 15 30 45 70 90
do

  # User info output

  echo "running ${theta}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Problem4.theta2 $theta -coarseSpaceActive false -eigenvectors 0 > out;


  # Extract data from output, write to CSV

  echo -n "${theta}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done





echo "AMG;" >> results.csv;

for theta in 0 1 3 5 10 15 30 45 70 90
do

  # User info output

  echo "running ${theta}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -solver amg -Problem4.theta2 $theta -coarseSpaceActive false -eigenvectors 0 -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "${theta}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

#fi

echo "geneo threshold;" >> results.csv;

for theta in 0 1 3 5 10 15 30 45 70 90
do

  # User info output

  echo "running ${theta}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Problem4.theta2 $theta -coarseSpaceActive true -eigenvectors 30 -threshold_eigenvectors true > out;


  # Extract data from output, write to CSV

  echo -n "${theta}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done





for eigenvectors in 1 10 15 20 #2 3 # 4 5 6 7 8 9 10
do

echo "geneo eigenvectors: ${eigenvectors};" >> results.csv;

for theta in 0 1 3 5 10 15 30 45 70 90
do

  # User info output

  echo "running ${theta}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Problem4.theta2 $theta -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "${theta}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

