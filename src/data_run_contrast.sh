#!/bin/sh


# Write first CSV row
echo "Contrast;Iterations;Condition;" > results.csv;




echo "AMG;" >> results.csv;


for contrast_i in 0 1 2 3 #4 6 9
do

  # User info output

  echo "running 1e${contrast_i}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver amg -coarseSpaceActive false -eigenvectors 0 -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.perm2 "1e${contrast_i}" > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done


if false; then


echo "one-level;" >> results.csv;

for contrast_i in 0 1 2 3
do

  # User info output

  echo "running 1e${contrast_i}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive false -eigenvectors 0 -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.perm2 "1e${contrast_i}" > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done




echo "ZEM;" >> results.csv;

for contrast_i in 0 1 2 3 #4 6 9
do

  # User info output

  echo "running 1e${contrast_i}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors 2 -threshold_eigenvectors false -ZEM_from_arpack true -Problem4.perm2 "1e${contrast_i}" > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done



echo "geneo threshold;" >> results.csv;

for contrast_i in 0 1 2 3 #4 6 9
do

  # User info output

  echo "running 1e${contrast_i}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors 5 -threshold_eigenvectors true -ZEM_from_arpack false -Problem4.perm2 "1e${contrast_i}" > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done



echo "part unity coarse space;" >> results.csv;

for contrast_i in 0 1 2 3 #4 6 9
do

  # User info output

  echo "running 1e${contrast_i}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors 0 -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.perm2 "1e${contrast_i}" > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done


fi

for eigenvectors in 1 2 3 4 5 6 7 8 9 10 11 12 #2 3 4 5
do

echo "geneo eigenvectors: ${eigenvectors};" >> results.csv;

for contrast_i in 0 1 2 3 #4 6 9
do

  # User info output

  echo "running 1e${contrast_i}";


  # Run program

  mpirun -np 25 ./dune-geneo -solver geneo -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false -ZEM_from_arpack false -Problem4.perm2 "1e${contrast_i}" > out;


  # Extract data from output, write to CSV

  echo -n "1e${contrast_i}" >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done

