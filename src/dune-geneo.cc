#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parametertree.hh>
Dune::ParameterTree configuration;

#include <dune/pdelab/boilerplate/pdelab.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include "gridtraits.hh"
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include "problem5.hh"
//#include "problem2.hh"
//#include "problem3.hh"
//#include "problem1_3D.hh"

#include "cg_fork.hh"
#include "cg_fork_inf_norm.hh"
#include "conbase_fork.hh"
#include <dune/geneo/geneo.hh>

#include "neumann_boundary_condition.hh"

//#include <dune/geneo/rndgeneobasis.hh>


template <typename GV, typename RF, int dim, typename Problem>
class ParameterGridFunction :
  public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, ParameterGridFunction<GV,RF,dim,Problem> >
{

public:

  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

  ParameterGridFunction (const GV& gv, const Problem& problem) : _problem(problem) {}


  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    y = _problem.A(e,xlocal).frobenius_norm();
    return;
  }

private:

  const Problem& _problem;
};



int main(int argc, char **argv)
{
  using Dune::PDELab::Backend::native;

  try{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc,argv);

  Dune::ParameterTreeParser parser;
  parser.readINITree("config.ini", configuration);
  parser.readOptions(argc, argv, configuration);


  int cells = configuration.get<int>("Grid.cells", 10);
  int overlap = configuration.get<int>("Grid.overlap", 1);

  // define parameters
  const unsigned int dim = 2;
  const unsigned int degree = 1;
  const std::size_t nonzeros = std::pow(2*degree+1,dim);
  const Dune::GeometryType::BasicType elemtype = Dune::GeometryType::cube;
  const Dune::PDELab::MeshType meshtype = Dune::PDELab::MeshType::conforming;
  const Dune::SolverCategory::Category solvertype = Dune::SolverCategory::overlapping;
  typedef double NumberType;

  // build a grid
  typedef Dune::YaspGrid<dim> GM;

  Dune::FieldVector<double,dim> L(1.0);
  Dune::array<int,dim> N(Dune::fill_array<int,dim>(cells));

  if (dim > 2)
    L[2] = helper.size();
  if (dim > 2)
    N[2] *= helper.size();
  std::bitset<dim> B(false);

  // instantiate the grid
  typedef Dune::YaspFixedSizePartitioner<dim> YP;
  std::array<int,dim> yasppartitions;
  yasppartitions[0] = configuration.get<int>("Grid.partition_x",1);
  if(dim > 1)
      yasppartitions[1] = configuration.get<int>("Grid.partition_y",1);
  if (dim > 2)
    yasppartitions[2] = configuration.get<int>("Grid.partition_z",1);
  auto yp = new YP(yasppartitions);

  auto grid = std::make_shared<GM>(L,N,B,overlap,Dune::MPIHelper::getCollectiveCommunication(),yp);


  grid->loadBalance();
  int J = grid->maxLevel();

// make random field
/*  MPI_Comm mpi_comm_local;
  MPI_Comm_split (MPI_COMM_WORLD, helper.rank(), 0, &mpi_comm_local);
if (helper.rank()==0) {
std::cout << "---------------WRITING" << std::endl;

 Dune::ParameterTree config;
 Dune::ParameterTreeParser parserConfig;
 parserConfig.readINITree("randomfield2d.ini",config);
 typedef GridTraits<double,double,2> GridTraits;

 Dune::RandomField::RandomFieldList<GridTraits> randomFieldList(config, "", Dune::RandomField::DefaultLoadBalance<dim>(), mpi_comm_local);

 randomFieldList.generate(true);
//if (helper.rank()==0) {
 randomFieldList.writeToFile("prueba");
std::cout << "---------------WRITING done" << std::endl;
 }
 MPI_Barrier (MPI_COMM_WORLD);*/
 //} else exit(0);

//std::cout << "---------------pass" << std::endl;

  // make problem parameters
  typedef GenericEllipticProblem<GM::LevelGridView,NumberType> Problem;
  Problem problem;//(mpi_comm_local);//grid->levelGridView(grid->maxLevel())
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> BCType;
  BCType bctype(grid->levelGridView(J),problem);


  // make a finite element space
  typedef typename GM::LevelGridView GV;
  typedef typename GM::ctype ctype;
  static const int dimworld = GM::dimensionworld;

  std::string solver_option = configuration.get<std::string>("solver", "");
  if (solver_option == "geneo") {


  typedef Dune::PDELab::CGFEMBase<GV,ctype,NumberType,degree,dim,elemtype> FEMB;
  typedef Dune::PDELab::CGCONBase<GM,degree,elemtype,meshtype,solvertype,BCType> CONB;

  typedef typename FEMB::FEM FEM;
  typedef typename CONB::CON CON;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;

  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  auto gv = grid->levelGridView(grid->maxLevel());
  auto fem = FEMB(gv);
  auto con = CONB(*grid, bctype);
  auto gfs = GFS(gv, fem.getFEM(), con.getCON()); gfs.name("Solution");
  con.postGFSHook(gfs);
  typedef typename GFS::template ConstraintsContainer<NumberType>::Type CC;
  auto cc = CC();
  auto cc_exterior = CC();
  auto cc_bnd_neu_int_dir = CC();

  /*typedef ParameterGridFunction<GV,ctype,dim,Problem> ParamGridFct;
  ParamGridFct parameter_grid_fct (gv, problem);

  {
    Dune::VTKWriter<GV> vtkwriter(gfs.gridView());
    //typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
    //DGF xdgf(gfs,x);
    typedef Dune::PDELab::VTKGridFunctionAdapter<ParamGridFct> ADAPT;
    auto adapt = std::make_shared<ADAPT>(parameter_grid_fct,"coefficients");
    vtkwriter.addVertexData(adapt);
    vtkwriter.write("parameters");
  }*/

  // make a degree of freedom vector on fine grid and initialize it with a function

  typedef Dune::PDELab::Backend::Vector<GFS,NumberType> V;
  V x(gfs,0.0);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
  G g(grid->levelGridView(J),problem);
  Dune::PDELab::interpolate(g,gfs,x);


  // assemble constraints
  cc.clear();
  Dune::PDELab::constraints(bctype,gfs,cc);
  cc_exterior.clear();
  if (configuration.get<bool>("EVboundariesFromProblem", ""))
    Dune::PDELab::constraints_exterior(bctype,gfs,cc_exterior);

  Dune::PDELab::PureNeumannBoundaryCondition pnbc;
  cc_bnd_neu_int_dir.clear();
  Dune::PDELab::constraints(pnbc,gfs,cc_bnd_neu_int_dir);

  // set initial guess
  V x0(gfs,0.0);
  {
    Dune::PDELab::copy_nonconstrained_dofs(cc,x0,x);
    con.make_consistent(gfs,x);
  }

  // assembler for finite element problem
  typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;
  LOP lop(problem);

  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,NumberType,NumberType,NumberType,CC,CC> GO;
  auto go = GO(gfs,cc,gfs,cc,lop,MBE(nonzeros));

  // set up and assemble right hand side w.r.t. l(v)-a(u_g,v)
  V d(gfs,0.0);
  go.residual(x,d);

  // now solve defect equation A*v = d
  V v(gfs,0.0);

  // types
  typedef GO::Jacobian M;

  // fine grid objects
  M AF(go);
  AF = 0.0;
  go.jacobian(x,AF); // assemble fine grid matrix
  typedef Dune::PDELab::OverlappingOperator<CC,M,V,V> POP;
  auto popf = std::make_shared<POP>(cc,AF);
  typedef Dune::PDELab::istl::ParallelHelper<GFS> PIH;
  PIH pihf(gfs);
  typedef Dune::PDELab::OverlappingScalarProduct<GFS,V> OSP;
  OSP ospf(gfs,pihf);

  // Compute fully Neumann system
  auto cc_full_neumann = CC();
  cc_full_neumann.clear();
  auto go_neumann = GO(gfs,cc_exterior,gfs,cc_exterior,lop,MBE(nonzeros));
  M AF_neumann(go_neumann);
  AF_neumann = 0.0;
  go_neumann.jacobian(x,AF_neumann); // assemble fine grid matrix

  // Compute overlap system (also Neumann)
  typedef Dune::PDELab::LocalOperatorOvlpRegion<LOP, GFS> LOP_OVLP;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP_OVLP,MBE,NumberType,NumberType,NumberType,CC,CC> GO_OVLP;
  LOP_OVLP lop_ovlp(lop, gfs);
  //auto go_overlap = GO_OVLP(gfs,cc_full_neumann,gfs,cc_full_neumann,lop_ovlp,MBE(nonzeros));
  auto go_overlap = GO_OVLP(gfs,cc_exterior,gfs,cc_exterior,lop_ovlp,MBE(nonzeros));
  M AF_ovlp(go_overlap);
  AF_ovlp = 0.0;
  go_overlap.jacobian(x,AF_ovlp); // assemble fine grid matrix

  //double eigenvalue_threshold = 2 * (double)overlap / (cells + 2 * overlap);
  double eigenvalue_threshold = (double)overlap / (cells + overlap);

  int verb=0;
  if (gfs.gridView().comm().rank()==0) verb=2;
  typedef Dune::PDELab::LocalFunctionSpace<GFS, Dune::PDELab::AnySpaceTag> LFS;
  LFS lfs(gfs);

  std::shared_ptr<V> part_unity;
  if (configuration.get<bool>("widlund_part_unity",false))
    part_unity = sarkisPartitionOfUnity<dim,V>(gfs, lfs, cc_bnd_neu_int_dir);
  else
    part_unity = standardPartitionOfUnity<dim,V>(gfs, lfs, cc_bnd_neu_int_dir);

  if (configuration.get<bool>("PartitionVTK",false)) {
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gfs.gridView(),0);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,*part_unity);
    vtkwriter.write("partition",Dune::VTK::appendedraw);
  }

  int nev = configuration.get<int>("eigenvectors",1);
  int nev_arpack = configuration.get<int>("eigenvectors_compute",-1);

  std::shared_ptr<SubdomainBasis<V> > subdomain_basis;
  if (nev > 0) {
    /*if (configuration.get<bool>("randomized_eigensolver"))
      subdomain_basis = std::make_shared<RndGenEOBasis<GFS,LFS,CC,M,V,V,1> >(gfs, lfs, cc, AF, AF_neumann, AF_ovlp, eigenvalue_threshold, *part_unity, nev, nev_arpack);
    else*/
    if(configuration.get<bool>("geneo",true))
        subdomain_basis = std::make_shared<GenEOBasis<GFS,M,V,V,1> >(gfs, AF_neumann, AF_ovlp, eigenvalue_threshold, *part_unity, nev, nev_arpack, configuration.get<double>("sigma",0.001));
    else
        subdomain_basis = std::make_shared<LBBasis<GFS,M,V,V,1> >(gfs, AF_neumann, AF_ovlp, eigenvalue_threshold, *part_unity, nev, nev_arpack, configuration.get<double>("sigma",0.001));
  } else if (nev == 0) {
    subdomain_basis = std::make_shared<PartitionOfUnityBasis<V> >(*part_unity);
  } else {
    subdomain_basis = std::make_shared<ZEMBasis<GFS,LFS,V,1,dim> >(gfs, lfs, *part_unity);
  }

  if (configuration.get<bool>("EigenvectorVTK",false)) {
    for (int i = 0; i < subdomain_basis->local_basis.size(); i++) {
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gfs.gridView(),0);
      V eigenvector(gfs, *subdomain_basis->local_basis[i]);
      Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,eigenvector);
      vtkwriter.write("eigenvector"+std::to_string(i),Dune::VTK::appendedraw);
    }
  }

  auto partunityspace = std::make_shared<SubdomainProjectedCoarseSpace<GFS,M,V,V,1> >(gfs, AF_neumann, subdomain_basis, verb);
  typedef TwoLevelOverlappingAdditiveSchwarz<GFS,M,V,V> PREC;
  auto prec = std::make_shared<PREC>(gfs, AF, partunityspace);

  //if (gfs.gridView().comm().rank() != 0)
  //  configuration["ConditionEstimate"] = "false";

if (!configuration.get<bool>("coarse_exclusive_solver",false)) {
  std::string solver_criterion = configuration.get<std::string>("criterion", "");
  if (solver_criterion == "defect") {


    Dune::InverseOperatorResult result;
    auto solver = std::make_shared<Dune::CGSolverFork<V> >(*popf,ospf,*prec,1E-6,1000,verb,true);
    solver->apply(v,d,result);

    // update x (which contains the Dirichlet conditions) by correction
    // which has zero Dirichlet BC
    x -= v;

  } else if (solver_criterion == "error") {
    V ref_soln = x;
    V v_ref = v;
    V d_ref = d;
    auto solver_ref = std::make_shared<Dune::CGSolverFork<V> >(*popf,ospf,*prec,1E-14,1000,verb);
    Dune::InverseOperatorResult result;
    solver_ref->apply(v_ref,d_ref,result);
    ref_soln -= v_ref;

    auto solver = std::make_shared<Dune::CGSolverForkInfNorm<V> >(*popf,ospf,*prec,1E-6,1000,verb);
    solver->apply(v,d,v_ref,ref_soln,result);

    // update x (which contains the Dirichlet conditions) by correction
    // which has zero Dirichlet BC
    x -= v;
  } else {
    std::cerr << "Unknown criterion!" << std::endl;
  }
} else {

  // Calculate reference solution (regular geneo)
  V ref_soln = x;
  V v_ref = v;
  V d_ref = d;
  auto solver_ref = std::make_shared<Dune::CGSolverFork<V> >(*popf,ospf,*prec,1E-6,1000,verb,true);
  Dune::InverseOperatorResult result;
  solver_ref->apply(v_ref,d_ref,result);
  ref_soln -= v_ref;


  // Calculate pure coarse solution

  //prec->pre(v,v);

  typedef typename PREC::COARSE_V COARSE_V;
  typedef typename PREC::COARSE_M COARSE_M;
  std::shared_ptr<COARSE_V> coarse_defect = partunityspace->restrict_defect (d);
  COARSE_V v0(partunityspace->basis_size(),partunityspace->basis_size());

  //Dune::InverseOperatorResult result;
  Dune::UMFPack<COARSE_M> coarse_solver (*partunityspace->get_coarse_system());
  coarse_solver.apply(v0,*coarse_defect,result);

  auto fine_correction = partunityspace->prolongate_defect (v0);

  Dune::PDELab::AddDataHandle<GFS,V> result_addh(gfs,*fine_correction);
  gfs.gridView().communicate(result_addh,Dune::All_All_Interface,Dune::ForwardCommunication);

  x -= *fine_correction;

  // Calculate error norm
  typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
  DGF dgf_ref(gfs, ref_soln);
  DGF dgf_soln(gfs, x);

  typedef Dune::PDELab::DifferenceAdapter<DGF,DGF> Difference;
  Difference error_diff(dgf_ref,dgf_soln);

  Dune::VTKWriter<GV> vtkwriter(gfs.gridView());
  typedef Dune::PDELab::VTKGridFunctionAdapter<Difference> ADAPT;
  auto adapt = std::make_shared<ADAPT>(error_diff,"error");
  vtkwriter.addVertexData(adapt);
  vtkwriter.write("error");


  typedef Dune::PDELab::DifferenceSquaredAdapter<DGF,DGF> DifferenceSquared;
  DifferenceSquared differencesquared(dgf_ref,dgf_soln);
  typename DifferenceSquared::Traits::RangeType error_norm_squared(0.0);
  Dune::PDELab::integrateGridFunction (differencesquared, error_norm_squared, 10);

  V zero_dummy (gfs, 0.0);
  DGF dgf_zero_dummy(gfs, zero_dummy);
  DifferenceSquared ref_squared(dgf_ref,dgf_zero_dummy);
  typename DifferenceSquared::Traits::RangeType ref_norm_squared(0.0);
  Dune::PDELab::integrateGridFunction (ref_squared, ref_norm_squared, 10);

  // Accumulate error norm across subdomains
  double error_norm_squared_global = gfs.gridView().comm().sum(error_norm_squared);
  double ref_norm_squared_global = gfs.gridView().comm().sum(ref_norm_squared);
  if (gfs.gridView().comm().rank() == 0)
    std::cout << "Error norm: " << std::sqrt(error_norm_squared_global) / std::sqrt(ref_norm_squared_global) << std::endl;


  typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> ADAPT2;
  auto adapt2 = std::make_shared<ADAPT2>(dgf_ref,"ref");
  auto adapt3 = std::make_shared<ADAPT2>(dgf_soln,"soln");
  vtkwriter.addVertexData(adapt2);
  vtkwriter.addVertexData(adapt3);
  vtkwriter.write("error");
}

  Dune::VTKWriter<GV> vtkwriter(gfs.gridView());
  typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
  DGF xdgf(gfs,x);
  typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> ADAPT;
  auto adapt = std::make_shared<ADAPT>(xdgf,"bla");
  vtkwriter.addVertexData(adapt);
  vtkwriter.write("geneo");


  } else if (solver_option == "amg") {

    // make a finite element space
    typedef Dune::PDELab::CGSpace<GM,NumberType,degree,BCType,elemtype,meshtype,solvertype> FS;
    FS fs(*grid,bctype);

    // make a degree of freedom vector and initialize it with a function
    typedef FS::DOF V;
    V x(fs.getGFS(),0.0);
    typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
    G g(grid->levelGridView(J),problem);
    Dune::PDELab::interpolate(g,fs.getGFS(),x);

    // assemble constraints
    fs.assembleConstraints(bctype);
    fs.setNonConstrainedDOFS(x,0.0);

    // assembler for finite elemenent problem
    typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FS::FEM> LOP;
    LOP lop(problem);
    typedef Dune::PDELab::GalerkinGlobalAssembler<FS,LOP,solvertype> ASSEMBLER;
    ASSEMBLER assembler(fs,lop, nonzeros);

    // make linear solver and solve problem
    //typedef Dune::PDELab::ISTLSolverBackend_IterativeDefault<FS,ASSEMBLER,solvertype> SBE;
    typedef Dune::PDELab::ISTLSolverBackend_CG_AMG_SSOR<FS,ASSEMBLER,solvertype> SBE;
    SBE sbe(fs,assembler,5000,2);
    typedef Dune::PDELab::StationaryLinearProblemSolver<ASSEMBLER::GO,SBE::LS,V> SLP;
    SLP slp(*assembler,*sbe,x,1e-6);
    slp.apply();

    // output grid to VTK file
    Dune::SubsamplingVTKWriter<GM::LeafGridView> vtkwriter(grid->leafGridView(),degree-1);
    FS::DGF xdgf(fs.getGFS(),x);
    vtkwriter.addVertexData(std::make_shared<FS::VTKF>(xdgf,"x_h"));
    vtkwriter.write("amg",Dune::VTK::appendedraw);

  } else {
    std::cout << "UNKNOWN SOLVER SPECIFIED!" << std::endl;
  }


  return 0;
  }

  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }

}
