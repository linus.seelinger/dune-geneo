#!/bin/sh


global_settings="
-solver geneo
-coarse_exclusive_solver true

-coarseSpaceActive true
-eigenvectors 0
-eigenvectors_compute 64
-threshold_eigenvectors false
-EVboundariesFromProblem true
-AddPartUnityToEigenvectors true

-OldMatrixSetup false
-widlund_part_unity false

-ConditionEstimate false
-criterion defect
-OrthogonalizeCG false


-Grid.cells 125
-Grid.overlap 1
-Grid.partition_x 5
-Grid.partition_y 5
-Grid.partition_z 1


-Problem1.perm1 1.0
-Problem1.perm2 1.0
-Problem1.layers 25

-Problem4.perm1 1.0
-Problem4.perm2 1e3
-Problem4.layers 25
-Problem4.theta1 0.0
-Problem4.theta2 5.0
-Problem4.eps1 1.0
-Problem4.eps2 1e-5

"
# Werte für die meisten Testläufe
#-Problem1.perm1 1.0
#-Problem1.perm2 1e3
#-Problem1.layers 25



# Write first CSV row
echo "Iterations;Error norm" > results.csv;

if false; then


echo "one-level;" >> results.csv;

#for contrast in 0 1 2 3 4 5
#do

  # User info output

  #echo "running 1e${contrast}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -ConditionEstimate true -coarseSpaceActive false -eigenvectors 0 > out;


  # Extract data from output, write to CSV

  #echo -n "1e${contrast}" >> results.csv;
  #echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo ";" >> results.csv;

  #echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  #echo ";" >> results.csv;

#done


echo "AMG;" >> results.csv;

#for contrast in 0 1 2 3 4 5
#do

  # User info output

  #echo "running 1e${contrast}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -solver amg -coarseSpaceActive false -eigenvectors 0 > out;


  # Extract data from output, write to CSV

  #echo -n "1e${contrast}" >> results.csv;
  #echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  echo ";" >> results.csv;

#done


#fi



echo "geneo threshold;" >> results.csv;

#for contrast in 0 1 2 3 4 5
#do

  # User info output

  #echo "running 1e${contrast}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -coarseSpaceActive true -eigenvectors 15 -threshold_eigenvectors true > out;


  # Extract data from output, write to CSV

  #echo -n "1e${contrast}" >> results.csv;
  #echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  echo -n ";" >> results.csv;

  #echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  #echo ";" >> results.csv;

#done


fi

for overlap in 2 4 8 #16 #1 2 5 10
do

echo "overlap ${overlap}" >> results.csv;


echo "geneo spectral basis" >> results.csv;

for eigenvectors in 40 #1 2 4 8 16 32 #64 #1 2 3 4 5 6 7 8 9 10 15 20
do

  # User info output

  echo "running ${eigenvectors}";


  # Run program

  mpirun -np 25 ./dune-geneo $global_settings -Grid.overlap ${overlap} -coarseSpaceActive true -eigenvectors ${eigenvectors} -threshold_eigenvectors false > out;


  # Extract data from output, write to CSV

  echo -n "${eigenvectors};" >> results.csv;

  #echo -n "1e${contrast}" >> results.csv;
  #echo -n ";" >> results.csv;

  #echo -n $(grep -Po "(?<= IT=).*" out) >> results.csv;
  #echo -n ";" >> results.csv;

  #echo -n $(grep -Po "(?<=Condition: ).*" out) >> results.csv;
  #echo -n ";" >> results.csv;

  echo -n $(grep -Po "(?<=Error norm: ).*" out) >> results.csv;
  echo ";" >> results.csv;

done

done


#fi


